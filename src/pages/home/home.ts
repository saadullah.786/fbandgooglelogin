import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Facebook, FacebookLoginResponse } from '@ionic-native/facebook';
import { LoginGooglePage } from '../login-google/login-google';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  mydata:any;
  profile:any;

  constructor(public navCtrl: NavController, private fb: Facebook) {

  }

  login(){
    this.fb.login(['public_profile', 'user_friends', 'email'])
  .then((res: FacebookLoginResponse) => 
  {
    alert(JSON.stringify(res));
    this.fb.api('me?fields=id,name,email,first_name,picture.width(720).height(720).as(picture_large)', []).then(profile => {
      alert('profile is '+JSON.stringify(profile))
      this.profile = profile;
      this.mydata = {email: profile.email, first_name: profile.first_name, picture: profile.picture_large.data.url, username: profile.name}
      alert('my data is '+JSON.stringify(this.mydata));
    });
  })
  .catch(e => alert('Error logging into Facebook'+ JSON.stringify(e)));

  }
  logout(){
    this.fb.logout().then(data => {
      alert('logout'+JSON.stringify(data))
    }).catch(err => {
      alert('logout error '+JSON.stringify(err));
    })
  }

  googlelogin(){
    this.navCtrl.push(LoginGooglePage);
  }

}
