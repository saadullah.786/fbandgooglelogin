import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { GooglePlus } from '@ionic-native/google-plus';

/**
 * Generated class for the LoginGooglePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */


@Component({
  selector: 'page-login-google',
  templateUrl: 'login-google.html',
})
export class LoginGooglePage {

  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
              public googlePlus:GooglePlus) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginGooglePage');
  }
  logingoogle(){
    this.googlePlus.login({})
  .then(res => alert('google login data '+JSON.stringify(res)))
  .catch(err => alert('google login error '+JSON.stringify(err)));
  }
  logoutgoogle(){
    this.googlePlus.logout().then(() => {
      alert('logout')
    }).catch(err => {
      alert('error is '+JSON.stringify(err));
    })
  }

}
